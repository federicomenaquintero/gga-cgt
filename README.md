# GGA-CGT

Grouping Genetic Algorithm with Controlled Gene Transmission for the bin packing
problem.

I didn't write the algorithm, I'm porting it to the Rust programming language.

## C version

Enter the `reference/` directory, the source is in `GGA-CGT.cpp`. Build with
make:

    make debug

or

    make main

and run the generated executable:

    ./debug

## Rust version

If it compiled, it would be as easy as

    cargo run --release

and it has some command line options, but it is not finished :P
