//! # GGA-CGT
//!
//! GROUPING GENETIC ALGORITHM WITH CONTROLLED GENE TRANSMISSION FOR THE BIN
//! PACKING PROBLEM
//!
//! Author: Marcela Quiroz-Castellanos
//! qc.marcela@gmail.com
//! Tecnológico Nacional de México
//! Instituto Tecnológico de Ciudad Madero
//! División de Estudios de Posgrado e Investigación
//! Depto. de Sistemas y Computación
//!
//! The program excecutes GGA-CGT over a set instances using different configurations
//! given by the user. Each configuration represents an independent execution of the GA.
//!
//! Reference:
//! Quiroz-Castellanos, M., Cruz-Reyes, L., Torres-Jimenez, J.,
//! Gómez, C., Huacuja, H. J. F., & Alvim, A. C. (2015).
//! A grouping genetic algorithm with controlled gene transmission for
//! the bin packing problem. Computers & Operations Research, 55, 52-64.
//!
//! Input:
//! File "instances.txt" including the name of the BPP instances to be solved;
//! Files including the standard instances to be solve;
//! File "configurations.txt" including the parameter values for each experiment;
//!
//! Output:
//! A set of files "GGA-CGT_(i).txt" including the experimental results for each
//! configuration i in the input, stored in directory: Solutions_GGA-CGT;
//! If(save_bestSolution = 1) a set of files HGGA_S_(i)_instance.txt including the
//! obtained solution for each instance, for each configuration i, stored in directory: Details_GGA-CGT;
use std::collections::HashSet;

use rand::{Rng, SeedableRng, seq::SliceRandom};
use serde::{Serialize, Deserialize};

pub mod sol;

use sol::{Individual, Bin, Index};

/// Describes a set of parameter settings for using in solving problem instances
#[derive(Copy, Clone, Deserialize)]
pub struct Config {
    /// How many individuals to keep in every iteration
    pub population_size: usize,

    /// Maximum number of iterations of the algorithm
    pub max_generations: usize,

    /// Individuals to mutate, percentage
    pub n_m: f64,

    /// Individuals to be recombined, percentage
    pub n_c: f64,

    /// Rate used to calculate the number of bins to eliminate. This is for when
    /// the solution is not cloned.
    pub k_ncs: f64,

    /// Rate used to calculate the number of bins to eliminate. This is for when
    /// the solution is cloned.
    pub k_cs: f64,

    /// Individuals in the elite set, percentage
    pub b_size: f64,

    /// Maximum age for individual to be cloned
    pub life_span: usize,

    /// Random seed
    pub seed: u64,
}

/// Describes the properties of a problem to solve.
#[derive(Debug, Serialize, Deserialize)]
pub struct Problem {
    /// The capacity of each bin.
    pub bin_capacity: u64,

    /// Is there a known best solution?
    pub known_best_solution: Option<usize>,

    /// The weights of the items to fit in the containers.
    pub weights: Vec<u64>,
}

/// Solves exactly one instance of the problem.
///
/// Frist item in the response indicates if the solution is optimal, second item
/// is the optimal solution if any or the best solution found.
pub fn solve(problem: Problem, config: Config) -> (bool, Individual) {
    let Problem {
        mut weights, bin_capacity, known_best_solution,
    } = problem;
    let Config {
        population_size, max_generations, seed, b_size, n_c, n_m, k_cs, k_ncs,
        life_span,
    } = config;

    // Sort weights in descending order
    weights.sort_unstable_by(|a, b| b.cmp(a));

    let mut rng = mt19937::MT19937::seed_from_u64(seed);
    let (n_, l2) = lowerbound(&weights, bin_capacity);
    let (optimal_solution, mut population) = generate_initial_population(population_size, &weights, bin_capacity, n_, l2, &mut rng);

    if let Some(sol) = optimal_solution {
        (true, sol)
    } else {
        for g in 0..max_generations {
            match generation(&mut population, g + 1, b_size, n_c, &mut rng, &weights, bin_capacity, l2, n_m, life_span, k_cs, k_ncs) {
                GenerationResult::Optimal(sol) => {
                    return (true, sol);
                }
                GenerationResult::RepeatedFitness => {
                    break;
                }
                GenerationResult::NoSolution => {}
            }
        }

        (false, find_best_solution(&population).clone())
    }
}

/// To generate an initial population P of individuals with FF-ñ packing heuristic.
///
/// population[i].bin_fullness: Saves the fitness of the solution i
/// population[i].num_bins: Saves the total number of bins in the solution i
/// population[i].generation: Saves the generation in which the solution i was generated
/// population[i].num_full_bins: Saves the number of bins in the solution i that are fully at 100%
/// population[i].fullness_of_emptiest_bin: Saves the fullness of the bin with the highest avaliable capacity in the solution i
/// Output:
/// (1) when it finds a solution for which the size matches the L2 lower bound
/// (0) otherwise
fn generate_initial_population<R>(size: usize, weights: &[u64], bin_capacity: u64, n_: usize, l2: usize, rng: &mut R) -> (Option<Individual>, Vec<Individual>)
where
    R: Rng,
{
    let population: Vec<_> = (0..size).map(|_| ff_n_(weights, bin_capacity, n_, rng)).collect();
    let optimal = population.iter().find(|s| s.bins.len() == l2).map(|s| s.clone());

    (optimal, population)
}

/// The result of each generation
pub enum GenerationResult {
    /// An optimal solution was found, must stop iterating.
    Optimal(Individual),

    /// More than 10% of the population has repeated fitness, must stop
    /// iterating.
    RepeatedFitness,

    /// Nothing interesting was found, must continue to the next generation.
    NoSolution,
}

pub use GenerationResult::Optimal;

/// To apply the reproduction technique: Controlled selection and Controlled replacement.
/// Output:
/// (1) when it finds a solution for which the size matches the L2 lower bound
/// (2) if more than 0.1*population.len() individuals (solutions) have duplicated-fitness
/// (0) otherwise
fn generation<R>(
    population: &mut [Individual], gen: usize, b_size: f64, n_c: f64, rng: &mut R,
    weights: &[u64], bin_capacity: u64, l2: usize, n_m: f64, life_span: usize,
    k_cs: f64, k_ncs: f64,
) -> GenerationResult
where
    R: Rng,
{
    //--------------------------------------------------------------------------
    //---------------------------------Controlled selection for crossover-------
    //--------------------------------------------------------------------------
    // sort population ascending by its fitness
    population.sort_unstable_by(|a, b| a.bin_fullness().partial_cmp(&b.bin_fullness()).unwrap());

    // a shuffled set of indices to the best individuals in the population
    let best_individuals = {
        let mut individuals: Vec<usize> = (
            (
                ((1.0 - n_c) * population.len() as f64) as usize
            )..population.len()
        ).collect();
        individuals.shuffle(rng);
        individuals.into_iter()
    };

    // a shuffled set of indices to a random portion of the population
    let mut random_individuals = {
        let mut individuals: Vec<usize> = (
            0..(
                ((1.0 - b_size) * population.len() as f64) as usize
            )
        ).collect();
        individuals.shuffle(rng);
        individuals.into_iter()
    };

    // How many children to generate through crossover
    let population_to_combine = (n_c * population.len() as f64) as usize;
    // Pairs of indices (best, random) where is warrantied that best != random
    let pairs: Vec<_> = best_individuals
        .map(|b| (b, random_individuals.find(|&r| r != b).unwrap()))
        .take(population_to_combine / 2)
        .collect();

    let children: Vec<_> = pairs.iter().map(|(b, r)| {
            let f1 = &population[*b];
            let f2 = &population[*r];

            vec![
                gene_level_crossover_ffd(f1, f2, weights, bin_capacity, gen),
                gene_level_crossover_ffd(f2, f1, weights, bin_capacity, gen),
            ]
        })
        .flatten()
        .collect();

    for child in children.iter() {
        if child.bins.len() == l2 {
            return GenerationResult::Optimal(child.clone());
        }
    }

    //--------------------------------------------------------------------------
    //---------------------------------Controlled replacement for crossover-----
    //--------------------------------------------------------------------------
    // Replace n_c/2 of the randomly chosen solutions from the population with
    // the new children.
    let mut first_children = children;
    let second_children = first_children.split_off(population_to_combine / 2);

    for (r, child) in pairs.into_iter().map(|(_b, r)| r).zip(first_children) {
        population[r] = child;
    }

    // Here I might have found a discrepancy with the article, which establishes
    // that if there are individuals with the same fitness they should be
    // replaced first, then the rest replace the worst. The original code
    // doesn't actually replace individuals with repeated fitness.
    let mut i = 0;
    for child in second_children {
        // this loop is to avoid replacen other children that were just inserted
        while population[i].generation() == gen {
            i += 1;
        }

        population[i] = child;
    }

    //--------------------------------------------------------------------------
    //--------------------------------Controlled selection for mutation---------
    //--------------------------------------------------------------------------
    population.sort_unstable_by(|a, b| a.bin_fullness().partial_cmp(&b.bin_fullness()).unwrap());

    // find the number of items with the same low fitness
    let fullness_of_worst_solution = population.first().unwrap().bin_fullness();
    let repeated_fitness = population
        .iter()
        .map(|s| s.bin_fullness())
        .take_while(|f| *f == fullness_of_worst_solution)
        .count();

    if gen > 1 && repeated_fitness > (0.1 * population.len() as f64) as usize {
        return GenerationResult::RepeatedFitness;
    }

    //--------------------------------------------------------------------------
    //----------------------------------Controlled replacement for mutation-----
    //--------------------------------------------------------------------------
    let mut j = 0;
    let mut i = population.len() - 1;

    while i > population.len() - (n_m * population.len() as f64) as usize {
        if j < (population.len() as f64 * b_size) as usize && gen - population[i].generation() < life_span {
            population[j] = population[i].clone();

            adaptive_mutation_rp(&mut population[j], k_cs, gen, bin_capacity, rng, weights);

            if population[j].bins.len() == l2 {
                return GenerationResult::Optimal(population[j].clone());
            }

            j += 1;
        } else {
            adaptive_mutation_rp(&mut population[i], k_ncs, gen, bin_capacity, rng, weights);

            if population[i].bins.len() == l2 {
                return GenerationResult::Optimal(population[i].clone());
            }
        }

        i -= 1;
    }

    GenerationResult::NoSolution
}

fn order_bins<'a>(bin1: &'a Bin, bin2: &'a Bin) -> (&'a Bin, &'a Bin) {
    if bin1.fullness() > bin2.fullness() {
        (bin1, bin2)
    } else {
        (bin2, bin1)
    }
}

/// To recombine two parent solutions producing a child solution.
///
/// Input:
///
/// * `father_1`: the first parent
/// * `father_2`: the second parent
/// * `weights`: the ordered weights of the problem
/// * `bin_capacity`: The capacity of the bin in this problem
/// * `gen`: generation where this solution was created
///
/// # Invariants
///
/// The solutions given as parents must have their bins ordered by fullness in
/// descending order
fn gene_level_crossover_ffd(
    father_1: &Individual, father_2: &Individual, weights: &[u64], bin_capacity: u64,
    gen: usize,
) -> Individual {
    let mut child_bins = Vec::with_capacity(father_1.bins.len().min(father_2.bins.len()));
    let mut unused_items: HashSet<Index> = (0..weights.len()).map(|a| a.into()).collect();

    // Here I made an important change. Assuming that, if a bin exists it is
    // because it contains an element, I removed the check for .fullness() > 0
    //
    // That change allows to see the symmetry of the if and else, from there it
    // was possible to imagine a function
    //
    // order(&bin1, &bin2) -> (&greater, &lower)
    //
    // that is used to remove the duplicated code
    for (f1bin, f2bin) in father_1.bins.iter().zip(father_2.bins.iter()) {
        let (greater, lower) = order_bins(f1bin, f2bin);

        if !used_items(greater, &child_bins) {
            child_bins.push(greater.clone());
            greater.remove_from_unused(&mut unused_items);
        }

        if !used_items(lower, &child_bins) {
            child_bins.push(lower.clone());
            lower.remove_from_unused(&mut unused_items);
        }
    }

    let mut sol = Individual::with_bins(child_bins, bin_capacity, gen);
    let mut bin_i = 0;

    for item in unused_items.into_iter() {
        ff(item, &mut sol, &mut bin_i, weights, bin_capacity);
    }

    sol.bins.sort_unstable_by(|a, b| b.fullness().cmp(&a.fullness()));

    sol
}

/// To produces a small modification in a solution.
///
/// Input:
/// * `individual`: The position in the population of the solution to mutate.
/// * `k`: The rate of change to calculate the number of bins to eliminate.
fn adaptive_mutation_rp<R>(
    individual: &mut Individual, k: f64, gen: usize, bin_capacity: u64, rng: &mut R,
    weights: &[u64],
)
where
    R: Rng,
{
    // Sort bins in descending order
    individual.bins.sort_unstable_by(|a, b| b.fullness().partial_cmp(&a.fullness()).unwrap());

    // The number of bins that are not full
    let nfb = individual.bins.iter().filter(|b| b.fullness() < bin_capacity).count();

    // The number of bins to be removed from the solution
    let _p_ = 1.0 / k;
    let number_bins = (
        nfb as f64 * (
            (2 - nfb / individual.bins.len()) as f64 / (nfb as f64).powf(_p_)
        ) * (
            1.0 - rng.gen_range(1..(((1.0 / (nfb as f64).powf(_p_)) * 100.0).ceil() as u64)) as f64 / 100.0
        )
    ).ceil() as usize;

    // Extract the specified bins from the solution
    let emptiest_bins = individual.bins.split_off(individual.bins.len() - number_bins);

    // Compute the set of free items from the removed bins
    let free_items: Vec<_> = emptiest_bins.into_iter().map(|b| b.items()).flatten().collect();

    rp(individual, free_items, weights, rng, bin_capacity, gen);
}

/// To generate a random BPP solution with the ñ large items packed in separate
/// bins.
///
/// Input:
///
/// The position in the population of the new solution: individual
fn ff_n_<R>(weights: &[u64], bin_capacity: u64, n_: usize, rng: &mut R) -> Individual
where
    R: Rng,
{
    let mut bin_i = 0;

    let (mut sol, mut permutation): (_, Vec<Index>) = if n_ > 0 {
        let bins: Vec<_> = (0..n_).map(|i| Bin::with_item(i.into(), weights)).collect();

        (
            Individual::with_bins(bins, bin_capacity, 0),
            (0..weights.len()).skip(n_).map(|a| a.into()).collect::<Vec<_>>()
        )
    } else {
        (
            Individual::new(),
            (0..weights.len()).map(|a| a.into()).collect::<Vec<_>>()
        )
    };

    permutation.shuffle(rng);

    for item in permutation {
        ff(item, &mut sol, &mut bin_i, weights, bin_capacity);
    }

    sol.bins.sort_unstable_by(|a, b| b.fullness().cmp(&a.fullness()));

    sol
}

/// To reinsert free items into an incomplete BPP solution.
///
/// Input:
/// * `individual`: The incomplete solution where the free items must be
///   reinserted.
/// * `free_items`: A set of free items to be reinserted into the partial
///   solution.
fn rp<R>(
    individual: &mut Individual, mut free_items: Vec<Index>, weights: &[u64],
    rng: &mut R, bin_capacity: u64, gen: usize,
) where
    R: Rng,
{
    let mut bins = individual.bins.split_off(0);

    bins.shuffle(rng);
    free_items.shuffle(rng);
    let number_free_items = free_items.len();

    // iterate over available bins, aparently in random order
    let bins: Vec<_> = bins.into_iter().map(|bin| {
        let (fullness, items) = bin.to_fullness_and_items();

        let mut sum = fullness;
        let mut bin_weights: Vec<_> = items.into_iter().collect();

        // For every weight in this container
        'outer: for p in 0..bin_weights.len() {
            // For every other weight in this container
            for s in (p + 1)..bin_weights.len() {
                // For every existing free item
                for k in 0..number_free_items {
                    // Si el k-ésimo peso libre pesa más que los pesos de p
                    // y s juntos y al reemplazar ambos por éste caben
                    // bien...
                    if weights[free_items[k].0 as usize] >= weights[bin_weights[p].0 as usize] + weights[bin_weights[s].0 as usize] &&
                        sum - (weights[bin_weights[p].0 as usize] + weights[bin_weights[s].0 as usize]) + weights[free_items[k].0 as usize] <= bin_capacity
                    {
                        // Actualiza el peso
                        sum = sum - (weights[bin_weights[p].0 as usize] + weights[bin_weights[s].0 as usize]) + (weights[free_items[k].0 as usize]);

                        // Libera p y s
                        let new_free_items_0 = bin_weights[p];
                        let new_free_items_1 = bin_weights[s];

                        // Mete el peso libre en p
                        bin_weights[p] = free_items[k];

                        // Saca a s de este contenedor
                        // TODO this operation can justify a different data
                        // structure, it might have been the justification of
                        // the original linked list
                        bin_weights.remove(s);

                        // Añade los elementos removidos al elemento de libres
                        free_items[k] = new_free_items_0;
                        free_items.push(new_free_items_1);

                        break 'outer;
                    }

                    // Por cada otro peso en los pesos libres
                    for k2 in (k + 1)..number_free_items {
                        // Si
                        //     la suma de los pesos libres k y k2 es mayor que la suma de los pesos p y s
                        //     o
                        //         La suma de los pesos libres es igual a p + s
                        //         y
                        //         k no es igual ni a p ni a s
                        if (weights[free_items[k].0 as usize] + weights[free_items[k2].0 as usize] > weights[bin_weights[p].0 as usize] + weights[bin_weights[s].0 as usize]) ||
                            (
                                (weights[free_items[k].0 as usize] + weights[free_items[k2].0 as usize] == weights[bin_weights[p].0 as usize] + weights[bin_weights[s].0 as usize]) &&
                                !(weights[free_items[k].0 as usize] == weights[bin_weights[p].0 as usize] || weights[free_items[k].0 as usize] == weights[bin_weights[s].0 as usize])
                            )
                        {
                            // Si no caben k y k2 en lugar de p y s
                            if sum - (weights[bin_weights[p].0 as usize] + weights[bin_weights[s].0 as usize]) + (weights[free_items[k].0 as usize] + weights[free_items[k2].0 as usize]) > bin_capacity {
                                // TODO This was a break, but I think it should be a continue
                                break;
                            }

                            sum = sum - (weights[bin_weights[p].0 as usize] + weights[bin_weights[s].0 as usize]) + (weights[free_items[k].0 as usize] + weights[free_items[k2].0 as usize]);
                            let new_free_items_0 = bin_weights[p];
                            let new_free_items_1 = bin_weights[s];
                            bin_weights[p] = free_items[k];
                            bin_weights[s] = free_items[k2];
                            free_items[k] = new_free_items_0;
                            free_items[k2] = new_free_items_1;

                            if sum == bin_capacity {
                                break 'outer;
                            }
                        }
                    } // for loop over free items, starting from where the previous loop is
                } // for loop over free items
            } // while loop over items of this bin from p
        } // while loop over items of this bin

        Bin::with_items(bin_weights, sum)
    }).collect(); // for loop over bins

    individual.bins.extend(bins);

    // Recompute all of individual's invariants

    // TODO compute a good value for bin_i, which perhaps can be computed with
    // the individual invariants
    let mut bin_i = 0;

    // Sort free items in ascending order
    free_items.sort_by_key(|&f| weights[f.0 as usize]);

    let higher_weight = weights[free_items.last().unwrap().0 as usize];
    let lighter_weight = weights[free_items.first().unwrap().0 as usize];

    if higher_weight < bin_capacity / 2 {
        free_items.shuffle(rng);
    }

    individual.recompute_invariants(bin_capacity);
    individual.bins.sort_unstable_by(|a, b| b.fullness().cmp(&a.fullness()));

    if lighter_weight > bin_capacity - individual.fullness_of_emptiest_bin() {
        bin_i = individual.bins.len();
    }

    // reacomodate missing items
    for item in free_items {
        ff(item, individual, &mut bin_i, weights, bin_capacity);
    }

    individual.set_generation(gen);
}

/// To insert an item into an incomplete BPP solution.
///
/// Input:
///
/// * item: The index of the weight to be inserted in the solution
/// * individual: An incomplete chromosome where the item must be inserted
/// * beginning: The first bin that could have sufficient available capacity to
///   store the item
fn ff(item: Index, individual: &mut Individual, beginning: &mut usize, weights: &[u64], bin_capacity: u64) {
    let pos = if weights[item.0 as usize] > (bin_capacity - individual.fullness_of_emptiest_bin()) {
        None
    } else {
        individual.bins
            .iter()
            .enumerate()
            .skip(*beginning)
            .find(|(_i, b)| b.fullness() + weights[item.0 as usize] <= bin_capacity)
            .map(|(i, _b)| i)
    };

    if let Some(i) = pos {
        individual.bins[i].add_item(item, weights);

        // if this bin's contents plus the smallest weight exceeds the bin
        // capacity update `beginning` which points to the first bin with enough
        // space for another item
        if individual.bins[i].fullness() + weights.last().unwrap() > bin_capacity && i == *beginning {
            *beginning += 1;
        }
    } else {
        individual.bins.push(Bin::with_item(item, weights));
    }

    // some invariants could be kept in this function, as it was in the original
    // code, but the solution's bin_fullness needs to account for the situation
    // where an item is added to an existing bin, thus the only clean solution
    // is to recompute it completely. Therefore we might as well just recompute
    // all invariants in the same loop.
    individual.recompute_invariants(bin_capacity);
}

/// To calculate the lower bound L2 of Martello and Toth and the ñ large items n_
fn lowerbound(weights: &[u64], bin_capacity: u64) -> (usize, usize) {
    let total_accumulated_weight: u64 = weights.iter().sum();
    let jx = weights.iter().filter(|&w| *w > bin_capacity / 2).count();
    let n_ = jx;

    if jx == weights.len() {
        return (n_, jx);
    }

    (n_, if jx == 0 {
        // TODO roughly translated, must check for accuracy
        if total_accumulated_weight % bin_capacity >= 1 {
            (total_accumulated_weight as f64 / bin_capacity as f64).ceil() as usize
        } else {
            (total_accumulated_weight / bin_capacity) as usize
        }
    } else {
        let cj12 = jx;
        let sjx: u64 = weights[jx..].iter().sum();

        let mut jp: usize = weights[..jx]
            .iter()
            .enumerate()
            .find(|(_i, &w)| w <= bin_capacity - weights[jx])
            .map(|(i, _w)| i)
            .unwrap_or(jx);

        let mut cj2 = jx - jp;
        let mut sj2: u64 = weights[jp..jx].iter().sum();
        let mut jpp = jx;
        let mut sj3: u64 = weights.iter().filter(|w| **w == weights[jpp]).sum();
        let mut l2 = cj12;

        loop {
            let aux1 = if (sj3 + sj2) % bin_capacity >= 1 {
                ((sj3 + sj2) as f64 / bin_capacity as f64 - (cj2 as f64)).ceil() as usize
            } else {
                ((sj3 + sj2) as f64 / bin_capacity as f64 - (cj2 as f64)) as usize
            };

            if l2 < (cj12 + aux1) {
                l2 = cj12 + aux1;
            }

            jpp += 1;

            if jpp < weights.len() {
                sj3 += weights[jpp];

                while jpp+1 < weights.len() && weights[jpp+1] == weights[jpp] {
                    jpp += 1;
                    sj3 += weights[jpp];
                }

                while jp > 0 && weights[jp-1] <= bin_capacity - weights[jpp] {
                    jp -= 1;
                    cj2 += 1;
                    sj2 += weights[jp];
                }
            }

            let aux2 = if (sjx + sj2) % bin_capacity >= 1 {
                ((sjx + sj2) as f64 / bin_capacity as f64 - (cj2 as f64)).ceil() as usize
            } else {
                ((sjx + sj2) as f64 / bin_capacity as f64 - (cj2 as f64)) as usize
            };

            if !(jpp <= weights.len() || (cj12 + aux2) > l2) {
                break;
            }
        }

        l2
    })
}

/// To find the solution with the highest fitness of the population and update the global_best_solution
fn find_best_solution(population: &[Individual]) -> &Individual {
    population
        .iter()
        .max_by(|a, b| a.bin_fullness().partial_cmp(&b.bin_fullness()).unwrap())
        .unwrap()
}

/// To check if any of the items of the current bin is already in the solution
///
/// Input:
/// The bin to check
/// An array that indicates the items that are already in the solution: items
///
/// Output:
/// (true) when some of the items in the current bin is already in the solution
/// (false) otherwise
fn used_items(bin: &Bin, used: &[Bin]) -> bool {
    used.iter().any(|b| b.intersects(bin))
}
