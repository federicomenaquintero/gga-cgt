use std::collections::HashSet;

use serde::Serialize;

#[derive(PartialEq, Eq, Hash, Copy, Clone, Serialize)]
pub struct Index(pub u16);

impl Into<Index> for usize {
    fn into(self: usize) -> Index {
        Index(self as u16)
    }
}

#[derive(Clone, Serialize)]
pub struct Bin {
    /// Occupancy of this bin
    fullness: u64,

    /// Indices in the weights array to the items contained in this bin
    pub items: HashSet<Index>,
}

impl Bin {
    /// Creates a new bin with a single item
    pub fn with_item(item: Index, weights: &[u64]) -> Bin {
        let mut items = HashSet::new();

        items.insert(item);

        Bin {
            items,
            fullness: weights[item.0 as usize],
        }
    }

    /// Creates a new bin from its items and precomputed fullness
    pub fn with_items(items: Vec<Index>, fullness: u64) -> Bin {
        Bin {
            fullness,
            items: items.into_iter().collect(),
        }
    }

    /// Adds an item to this bin, keeping the invariant of its fullness
    pub fn add_item(&mut self, item: Index, weights: &[u64]) {
        self.fullness += weights[item.0 as usize];
        self.items.insert(item);
    }

    pub fn fullness(&self) -> u64 {
        self.fullness
    }

    /// Checks wether the contents of the bin intersect the items given
    pub fn intersects(&self, other: &Bin) -> bool {
        !self.items.is_disjoint(&other.items)
    }

    pub fn remove_from_unused(&self, unused_items: &mut HashSet<Index>) {
        for item in self.items.iter() {
            unused_items.remove(&item);
        }
    }

    pub fn items(self) -> Vec<Index> {
        self.items.into_iter().collect()
    }

    pub fn to_fullness_and_items(self) -> (u64, HashSet<Index>) {
        (self.fullness, self.items)
    }
}

#[derive(Clone, Default, Serialize)]
pub struct Individual {
    /// Saves the fitness of the solution i
    bin_fullness: f64,

    /// Saves the generation in which the solution i was generated
    generation: usize,

    /// Saves the number of bins in the solution i that are fully at 100%
    num_full_bins: usize,

    /// Saves the fullness of the bin with the highest avaliable capacity in the solution i
    fullness_of_emptiest_bin: u64,

    /// the bins that this solution defines
    pub bins: Vec<Bin>,

}

impl Individual {
    /// Create a new empty solution
    pub fn new() -> Individual {
        Default::default()
    }

    /// Create a new solution with the given bins.
    ///
    /// this method ensures the invariants of a solution are kept.
    pub fn with_bins(bins: Vec<Bin>, bin_capacity: u64, generation: usize) -> Individual {
        let mut sol = Individual {
            bins,
            generation,
            ..Default::default()
        };

        sol.recompute_invariants(bin_capacity);

        sol
    }

    /// Keep the solution's invariants
    ///
    /// * update the number of full bins
    /// * recompute solution fitness (bin_fullness)
    /// * recompute fullness_of_emptiest_bin
    pub fn recompute_invariants(&mut self, bin_capacity: u64) {
        let (num_full_bins, bin_fullness, fullness_of_emptiest_bin) = self.bins.iter()
            .fold((0, 0.0, u64::MAX), |acc, b| {
                let (mut num_full_bins, mut bin_fullness, mut fullness_of_emptiest_bin) = acc;

                if b.fullness == bin_capacity {
                    num_full_bins += 1;
                }

                bin_fullness += (b.fullness as f64 / bin_capacity as f64).powi(2);

                if b.fullness < fullness_of_emptiest_bin {
                    fullness_of_emptiest_bin = b.fullness;
                }

                (num_full_bins, bin_fullness, fullness_of_emptiest_bin)
            });

        self.num_full_bins = num_full_bins;
        self.bin_fullness = bin_fullness / self.bins.len() as f64;
        self.fullness_of_emptiest_bin = fullness_of_emptiest_bin;
    }

    /// Returns this solution's fullness_of_emptiest_bin value
    pub fn fullness_of_emptiest_bin(&self) -> u64 {
        self.fullness_of_emptiest_bin
    }

    pub fn bin_fullness(&self) -> f64 {
        self.bin_fullness
    }

    pub fn generation(&self) -> usize {
        self.generation
    }

    pub fn set_generation(&mut self, gen: usize) {
        self.generation = gen;
    }
}

#[derive(Serialize)]
pub struct Solution {
    /// Seconds that took to compute the solution
    pub elapsed: f64,

    /// Wether or not this solution meets the L2 lower bound
    pub is_optimal: bool,

    /// The individual with the description of the solution
    pub individual: Individual,
}
