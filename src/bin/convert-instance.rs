use std::fs::{File, create_dir_all};
use std::io::{self, Write, BufReader, BufRead};
use std::path::Path;

#[macro_use]
extern crate clap;

use clap::{Arg, App};

use gga_cgt::Problem;

fn main() -> io::Result<()> {
    let matches = App::new("convert-instance")
        .version(crate_version!())
        .author(crate_authors!())
        .about("Converts an instance of a problem from the old format to the new")
        .arg(
            Arg::with_name("output")
                .short("o")
                .long("output")
                .value_name("DIR")
                .help("A directory where output of every case will be written (if it doesn't exist will be created)")
                .required(true)
                .takes_value(true)
        )
        .arg(
            Arg::with_name("INSTANCES")
               .help("Filenames representing the different instances to solve")
               .required(true)
               .multiple(true)
        )
        .get_matches();

    let output_dir = matches.value_of("output").unwrap();

    create_dir_all(output_dir)?;

    for filename in matches.values_of("INSTANCES").unwrap() {
        let file = File::open(filename)?;
        let buf = BufReader::new(file);

        let mut lines = buf.lines();

        // skip number of items header
        lines.next();

        let num_items: usize = lines.next().unwrap().unwrap().trim().parse().unwrap();

        // skip bin_capacity header
        lines.next();

        let bin_capacity: u64 = {
            let b: f64 = lines.next().unwrap().unwrap().trim().parse().unwrap();

            b as u64
        };

        // skip known_best_solution header
        lines.next();

        let known_best_solution: i64 = lines.next().unwrap().unwrap().trim().parse().unwrap();

        // skip weights header
        lines.next();

        let weights: Vec<u64> = (0..num_items).zip(lines).map(|(_i, l)| {
            let w: f64 = l.unwrap().trim().parse().unwrap();

            w as u64
        }).collect();

        let outputfilename = Path::new(filename).file_name().unwrap();
        let mut outputfile = File::create(Path::new(output_dir).join(outputfilename).with_extension("toml"))?;

        outputfile.write_all(toml::to_string(&Problem {
            bin_capacity,
            known_best_solution: if known_best_solution != -1 {
                Some(known_best_solution as usize)
            } else {
                None
            },
            weights,
        }).unwrap().as_bytes())?;
    }

    Ok(())
}
